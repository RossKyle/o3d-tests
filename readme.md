# O3D Point Cloud to Mesh Tests

_Author: Kyle Ross_

This script tests _Open3D_'s ball pivoting algorithm and poisson reconstruction algorithms to generate a mesh from a point cloud.

## Collab Notebook version

https://colab.research.google.com/drive/1oc8X78zi1q06cK8IRxPdUWV8aABCBrxm?usp=sharing

## References

1. https://towardsdatascience.com/5-step-guide-to-generate-3d-meshes-from-point-clouds-with-python-36bad397d8ba
